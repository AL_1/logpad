package me.diamonddev.logpad.register;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

import me.diamonddev.logpad.utlis.playerdata.DataManager;

public class RegisterListener implements Listener {

	HashMap<UUID, String> typedInCodes = new HashMap<UUID, String>();

	@EventHandler
	public void onItemClick(InventoryClickEvent e) {
		if (e.getInventory() == null) {
			return;
		}
		if (!e.getInventory().getTitle().contains(">")) {
			return;
		}
		if (Register.registering.contains(e.getWhoClicked().getUniqueId())) {
			if (e.getClickedInventory().getType() != InventoryType.PLAYER) {
				e.setCancelled(true);
				Player p = (Player) e.getWhoClicked();
				Inventory inv = e.getClickedInventory();

				Short[] numPadA = { 12, 13, 14, 21, 22, 23, 30, 31, 32, 40 };
				List<Short> numPad = Arrays.asList(numPadA);
				int redSlot = 41;
				int greenSlot = 39;
				int backSlot = 15;

				int clickedSlot = e.getSlot();

				if (!typedInCodes.containsKey(p.getUniqueId()))
					typedInCodes.put(p.getUniqueId(), "");
				if (numPad.contains((short) clickedSlot)) {
					int clickedNum = Integer
							.valueOf(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));

					if (typedInCodes.get(p.getUniqueId()).length() > 8) {
						return;
					}

					typedInCodes.put(p.getUniqueId(), typedInCodes.get(p.getUniqueId()) + clickedNum);
					Inventory newInv = Bukkit.createInventory(p, 54, inv.getTitle() + "*");
					newInv.setContents(inv.getContents());
					p.openInventory(newInv);
				} else if (clickedSlot == redSlot) {
					p.sendMessage(ChatColor.RED + "Registartation Canceled...");
					Register.confirmRegistering.remove(p.getUniqueId());
					typedInCodes.remove(p.getUniqueId());
					p.closeInventory();
					if (Register.registering.contains(p.getUniqueId()))
						Register.registering.remove(p.getUniqueId());
					if (Register.confirmRegistering.contains(p.getUniqueId()))
						Register.confirmRegistering.remove(p.getUniqueId());
				} else if (clickedSlot == backSlot) {
					if (typedInCodes.get(p.getUniqueId()).length() == 0)
						return;
					Inventory newInv = Bukkit.createInventory(p, 54,
							inv.getTitle().substring(0, inv.getTitle().length() - 1));
					typedInCodes.put(p.getUniqueId(), typedInCodes.get(p.getUniqueId()).substring(0,
							typedInCodes.get(p.getUniqueId()).length() - 1));
					newInv.setContents(inv.getContents());
					p.openInventory(newInv);
				} else if (clickedSlot == greenSlot) {
					String typedInPin = typedInCodes.get(p.getUniqueId());
					if (typedInPin == "" || typedInPin == " " || typedInPin == null || typedInPin.isEmpty())
						return;
					if (typedInPin.length() < 4)
						return;
					Register.confirmRegisterPin.put(p.getUniqueId(), typedInPin);
					typedInCodes.remove(p.getUniqueId());
					Register.confirmRegister(p);
				}
			}
		} else if (Register.confirmRegistering.contains(e.getWhoClicked().getUniqueId())) {
			if (e.getClickedInventory().getType() != InventoryType.PLAYER) {
				e.setCancelled(true);
				Player p = (Player) e.getWhoClicked();
				Inventory inv = e.getClickedInventory();

				if (!typedInCodes.containsKey(p.getUniqueId()))
					typedInCodes.put(p.getUniqueId(), "");

				Short[] numPadA = { 12, 13, 14, 21, 22, 23, 30, 31, 32, 40 };
				List<Short> numPad = Arrays.asList(numPadA);
				int redSlot = 41;
				int greenSlot = 39;
				int backSlot = 15;

				int clickedSlot = e.getSlot();

				if (numPad.contains((short) clickedSlot)) {
					int clickedNum = Integer
							.valueOf(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));

					if (typedInCodes.get(p.getUniqueId()).length() > 8) {
						return;
					}

					typedInCodes.put(p.getUniqueId(), typedInCodes.get(p.getUniqueId()) + clickedNum);
					Inventory newInv = Bukkit.createInventory(p, 54, inv.getTitle() + "*");
					newInv.setContents(inv.getContents());
					p.openInventory(newInv);
				} else if (clickedSlot == redSlot) {
					p.sendMessage(ChatColor.RED + "Registartation Canceled...");
					Register.confirmRegistering.remove(p.getUniqueId());
					typedInCodes.remove(p.getUniqueId());
					p.closeInventory();
					if (Register.registering.contains(p.getUniqueId()))
						Register.registering.remove(p.getUniqueId());
					if (Register.confirmRegistering.contains(p.getUniqueId()))
						Register.confirmRegistering.remove(p.getUniqueId());
				} else if (clickedSlot == backSlot) {
					if (typedInCodes.get(p.getUniqueId()).length() == 0)
						return;
					Inventory newInv = Bukkit.createInventory(p, 54,
							inv.getTitle().substring(0, inv.getTitle().length() - 1));
					typedInCodes.put(p.getUniqueId(), typedInCodes.get(p.getUniqueId()).substring(0,
							typedInCodes.get(p.getUniqueId()).length() - 1));
					newInv.setContents(inv.getContents());
					p.openInventory(newInv);
				} else if (clickedSlot == greenSlot) {
					String PlayersPin = Register.confirmRegisterPin.get(p.getUniqueId());
					String typedInPin = typedInCodes.get(p.getUniqueId());
					if (typedInPin == "" || typedInPin == " " || typedInPin == null || typedInPin.isEmpty())
						return;
					if (typedInPin.length() < 4)
						return;
					if (!PlayersPin.equalsIgnoreCase(typedInPin)) {
						p.sendMessage(ChatColor.RED + "Inncorrect confirmation pin!");
						p.closeInventory();
						Register.confirmRegistering.remove(p.getUniqueId());
					} else {
						DataManager.getPlayerData(p.getUniqueId()).setPin(typedInCodes.get(p.getUniqueId()));
						Register.confirmRegistering.remove(p.getUniqueId());
						p.sendMessage(ChatColor.GREEN + "Registation compleate!");
						p.sendMessage(ChatColor.GREEN + "Logged in!");
						p.closeInventory();
					}
					typedInCodes.remove(p.getUniqueId());
				}
			}
		}

	}

	/*
	 * @EventHandler public void onInvClose(InventoryCloseEvent e) { if
	 * (Register.registering.contains(e.getPlayer().getUniqueId())) {
	 * Register.registering.remove(e.getPlayer().getUniqueId());
	 * e.getPlayer().sendMessage(ChatColor.RED + "Registartation Canceled...");
	 * } if (Register.confirmRegistering.contains(e.getPlayer().getUniqueId()))
	 * { Register.confirmRegistering.remove(e.getPlayer().getUniqueId());
	 * e.getPlayer().sendMessage(ChatColor.RED + "Registartation Canceled...");
	 * } }
	 */

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		if (Register.registering.contains(e.getPlayer().getUniqueId()))
			Register.registering.remove(e.getPlayer().getUniqueId());
		if (Register.confirmRegistering.contains(e.getPlayer().getUniqueId()))
			Register.confirmRegistering.remove(e.getPlayer().getUniqueId());

	}
}
