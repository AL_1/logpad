package me.diamonddev.logpad.utlis.playerdata;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.UUID;

import org.bukkit.configuration.file.YamlConfiguration;

import me.diamonddev.logpad.Plugin;
import me.diamonddev.logpad.utlis.server.Log;

public class PlayerData {
	UUID uuid = UUID.randomUUID();
	YamlConfiguration data = null;
	File file = null;
	public static File datafolder = Plugin.gi().getDataFolder();

	public PlayerData(File ymlFile, UUID playerUUID) {
		this.file = ymlFile;
		this.uuid = playerUUID;
		data = YamlConfiguration.loadConfiguration(ymlFile);
		data.set("UUID", playerUUID.toString());
		try {
			URL whatismyip = new URL("http://www.http://voltiac.ml/services/JavaPlugin/username.php?uuid="
					+ playerUUID.toString().replaceAll("-", ""));
			BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
			data.set("Username", in.readLine());
		} catch (Exception e) {
		}
	}

	// Gold Control
	public void setPin(String pin) {
		data.set("Pin", pin);
		saveData();
	}

	public String getPin() {
		reloadData();
		return data.getString("Pin");
	}

	public boolean isPinSet() {
		reloadData();
		return data.contains("Pin");
	}

	// Data Control
	public void saveData() {
		try {
			data.save(file);
		} catch (IOException e) {
			Log.warning(null, "Could not save " + file.getName());
		}
	}

	public void reloadData() {
		data = YamlConfiguration.loadConfiguration(file);
	}

	public YamlConfiguration getData() {
		return data;
	}
}
