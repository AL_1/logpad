package me.diamonddev.logpad.utlis.server;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class Log {
	private static Logger log = Bukkit.getServer().getLogger();

	private static String getPregix(Plugin pl){
		if(pl==null){
			return "[DiamondDev] ";
		}
		return "[" + pl.getDescription().getName() + "] ";
	}
	
	public static void info(Plugin pl,Object obj) {
		log.info(getPregix(pl) + obj.toString());
	}

	public static void warning(Plugin pl,Object obj) {
		log.warning(getPregix(pl) + obj.toString());
	}

	public static void debug(Plugin pl, Level level, Object obj) {
		boolean debug = pl.getConfig().getBoolean("Options.Debug");
		if (debug)
			log.log(level, getPregix(pl) + obj.toString());
	}
}
