package me.diamonddev.logpad;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.diamonddev.logpad.register.Register;
import me.diamonddev.logpad.register.RegisterListener;
import me.diamonddev.logpad.signin.SignIn;
import me.diamonddev.logpad.signin.SignInListener;
import me.diamonddev.logpad.utlis.playerdata.DataCreator;

public class Plugin extends JavaPlugin {
	@Override
	public void onEnable() {
		// saveDefaultConfig();
		// reloadConfig();
		getServer().getPluginManager().registerEvents(new JoinListener(), this);
		getServer().getPluginManager().registerEvents(new DataCreator(), this);
		getServer().getPluginManager().registerEvents(new SignInListener(), this);
		getServer().getPluginManager().registerEvents(new RegisterListener(), this);
	}

	@Override
	public void onDisable() {
		saveConfig();
	}

	public static Plugin gi() {
		return (Plugin) Bukkit.getPluginManager().getPlugin("LogPad");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("login")) {
			if ((sender instanceof Player))
				SignIn.signIn((Player) sender);
			return true;
		} else if (label.equalsIgnoreCase("register")) {
			if ((sender instanceof Player))
				Register.register((Player) sender);
			return true;
		} else {
			sender.sendMessage(ChatColor.RED + "Error!");
			return true;
		}
	}
}
