package me.diamonddev.logpad.signin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

import me.diamonddev.logpad.utlis.playerdata.DataManager;

public class SignInListener implements Listener {

	HashMap<UUID, String> typedInCodes = new HashMap<UUID, String>();

	@EventHandler
	public void onItemClick(InventoryClickEvent e) {
		if (e.getInventory() == null) {
			return;
		}
		if (!e.getInventory().getTitle().contains(">")) {
			return;
		}
		if (SignIn.signingIn.contains(e.getWhoClicked().getUniqueId())) {
			if (e.getClickedInventory().getType() != InventoryType.PLAYER) {
				e.setCancelled(true);
				Player p = (Player) e.getWhoClicked();
				Inventory inv = e.getClickedInventory();

				Short[] numPadA = { 12, 13, 14, 21, 22, 23, 30, 31, 32, 40 };
				List<Short> numPad = Arrays.asList(numPadA);
				int redSlot = 41;
				int greenSlot = 39;
				int backSlot = 15;

				int clickedSlot = e.getSlot();

				if (!typedInCodes.containsKey(p.getUniqueId()))
					typedInCodes.put(p.getUniqueId(), "");
				if (numPad.contains((short) clickedSlot)) {
					int clickedNum = Integer
							.valueOf(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));

					if (typedInCodes.get(p.getUniqueId()).length() > 8) {
						return;
					}

					typedInCodes.put(p.getUniqueId(), typedInCodes.get(p.getUniqueId()) + clickedNum);
					Inventory newInv = Bukkit.createInventory(p, 54, inv.getTitle() + "*");
					newInv.setContents(inv.getContents());
					p.openInventory(newInv);
				} else if (clickedSlot == redSlot) {
					p.kickPlayer(ChatColor.GREEN + "Bye Bye! :P");
					SignIn.signingIn.remove(p.getUniqueId());
					typedInCodes.remove(p.getUniqueId());
					SignIn.signingIn.remove(p.getUniqueId());
				} else if (clickedSlot == backSlot) {
					if (typedInCodes.get(p.getUniqueId()).length() == 0)
						return;
					Inventory newInv = Bukkit.createInventory(p, 54,
							inv.getTitle().substring(0, inv.getTitle().length() - 1));
					typedInCodes.put(p.getUniqueId(), typedInCodes.get(p.getUniqueId()).substring(0,
							typedInCodes.get(p.getUniqueId()).length() - 1));
					newInv.setContents(inv.getContents());
					p.openInventory(newInv);
				} else if (clickedSlot == greenSlot) {
					String PlayersPin = DataManager.getPlayerData(p.getUniqueId()).getPin();
					String typedInPin = typedInCodes.get(p.getUniqueId());
					if (typedInPin == "" || typedInPin == " " || typedInPin == null)
						return;
					if (typedInPin.length() < 4)
						return;
					if (!PlayersPin.equalsIgnoreCase(typedInPin)) {
						p.kickPlayer(ChatColor.RED + "Invalid Pin!");
					} else {
						p.closeInventory();
						p.sendMessage(ChatColor.GREEN + "Logged in!");
					}
					SignIn.signingIn.remove(p.getUniqueId());
				}
			}
		}

	}

	public void onInvClose(InventoryCloseEvent e) {
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if (SignIn.signingIn.contains(e.getPlayer().getUniqueId())) {
			e.getPlayer().sendMessage(ChatColor.RED + "You must log on in order to move! Do \"/login\" to login!");
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onSpeak(AsyncPlayerChatEvent e) {
		if (SignIn.signingIn.contains(e.getPlayer().getUniqueId())) {
			e.getPlayer().sendMessage(ChatColor.RED + "You must log on in order to speak! Do \"/login\" to login!");
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().equalsIgnoreCase("/login"))
			return;
		if (SignIn.signingIn.contains(e.getPlayer().getUniqueId())) {
			e.getPlayer()
					.sendMessage(ChatColor.RED + "You must log on in order to run commands! Do \"/login\" to login!");
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		if (SignIn.signingIn.contains(e.getPlayer().getUniqueId())) {
			SignIn.signingIn.remove(e.getPlayer().getUniqueId());
		}
	}
}
